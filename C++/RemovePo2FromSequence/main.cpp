/*
 * Write a program that removes from a given sequence all
 * numbers which are power of 2.
 */

#include <iostream>
#include <vector>

#if __cplusplus >= 201103L
    #define CPLUSPLUS11
#endif // __cplusplus > 201103L


#define SEQUENCE_SIZE 20
int sequence[SEQUENCE_SIZE] = {2, 3, 4, 6, 7, 2, 4, 6, 5, 3, 4, 4, 6, 2, 3, 8, 3, 7, 7, 8};

///
/// \brief isPowerOf2
/// \param number
/// \return true if number is power of two
///
///  1 = 0x0001b
///  2 = 0x0010b
///  4 = 0x0100b
///  8 = 0x1000b
///
///  0 = 0x0000b
///  1 = 0x0001b
///  3 = 0x0011b
///  7 = 0x0111b
///
bool isPowerOf2(int number)
{
    if (number <= 0)
    {
        return false;
    }

    return !(number & (number - 1));
}

int main()
{
    std::vector<int> sequenceNew;

    for (int i = 0; SEQUENCE_SIZE > i; i++)
    {
        if (!isPowerOf2(sequence[i]))
        {
            sequenceNew.push_back(sequence[i]);
        }
    }

    std::cout << "Original sequence:" << std::endl;
    for (int& number: sequence)
    {
        std::cout << " " << number;
    }
    std::cout << std::endl;

#if defined CPLUSPLUS11
    std::cout << "New sequence with numbers equal to power of two removed:" << std::endl;
    for (int& number: sequenceNew)
    {
        std::cout << " " << number;
    }
    std::cout << std::endl;
#else
    std::cout << "New sequence with numbers equal to power of two removed (iterator):" << std::endl;
    for (std::vector<int>::iterator i = sequenceNew.begin();
         sequenceNew.end() != i;
         i++ )
    {
        std::cout << " " << *i;
    }
    std::cout << std::endl;
#endif // CPLUSPLUS11




    return 0;
}

