import qbs

CppApplication {
    type: "application" // To suppress bundle generation on Mac
    consoleApplication: true
    files: "main.cpp"
    cpp.cxxFlags: "-std=c++11" // Set C++ compiler flag to enable C++11 support

    Group {     // Properties for the produced executable
        fileTagsFilter: product.type
        qbs.install: true
    }
}

