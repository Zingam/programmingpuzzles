#include <iostream>
#include <vector>

#define LENGTH 35
#define TEST_NUMBER 3

int numbers[LENGTH] = {4, 5, 3, 4, 5, 2, 3, 5, 4, 2, 2, 4, 4, 9, 6, 4, 2, 1, 5, 7, 7, 8, 7, 4, 6, 2, 2, 1, 3, 5, 5, 5, 2, 4, 2};

template<typename T>
void printSequence(std::vector<T>& sequence)
{
    for (auto currentElement: sequence)
    {
        std::cout << currentElement << ", ";
    }
}

int main()
{
    int testNumber = TEST_NUMBER;

    std::vector<int> currentSequence;
    std::vector<std::vector<int>> sequences;

    for (auto currentNumber: numbers)
    {
        if (testNumber < currentNumber)
        {
            currentSequence.push_back(currentNumber);
        }
        else if (!currentSequence.empty())
        {
            sequences.push_back(currentSequence);
            currentSequence.clear();
        }
    }

    unsigned int longestSequenceIndex = 0;
    unsigned int longestSequenceLength = 0;

    for (unsigned int i = 0; i < sequences.size(); i++)
    {
        unsigned int currentSequenceLength = sequences.at(i).size();

        if ( currentSequenceLength > longestSequenceLength)
        {
            longestSequenceLength = currentSequenceLength;
            longestSequenceIndex = i;
        }

        std::cout << "Sequence No. " << i << ": ";
        printSequence(sequences.at(i));
        std::cout << std::endl;
    }

        std::cout << "Longest sequence found: ";
        printSequence(sequences.at(longestSequenceIndex));
        std::cout << std::endl;

    return 0;
}

